---
title: la Pizza Lectora
comments: true
date: 2018-06-16 10:00:00
photos:
- /images/Pizza_Dimension.png
---

Realizamos el tercer encuentro de la ahora llamada "La Pizza Lectora". Un
encuentro para hablar de libros mientras comemos pizza. En esta ocación
hablamos de 1984 de George Orwell, para seguir con el ciclo de futuros
distópicos.

La próxima reunión es el 20 de julio en Hackatory, Av. Freyre 3043 D4.

Esta es la [lista](https://hackatory.sandcats.io/shared/0NLPhefuJSMlpQk9AcRiP11eufVcK70WMxUPWZSEHa0)
votar por el próximas ediciones.
